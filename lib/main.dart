import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:job_portal_final/colors.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:job_portal_final/screens/check_role.dart';
import 'package:job_portal_final/shared/screens.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final storage = const FlutterSecureStorage();

  const MyApp({super.key});
  Future<bool> checkLoginStatus() async {
    String? value = await storage.read(key: "uid");
    if (value == null) {
      // Not logged in
      return false;
    }
    return true; //Logged In
  }

  @override
  Widget build(BuildContext context) {
    // final Future<FirebaseApp> _initialization = Firebase.initializeApp();

    return FutureBuilder(
      future: null,
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          print("Something went wrong");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        return MultiProvider(
          providers: [
            ChangeNotifierProvider(
              create: (context) => GoogleSignInController(),
              child: const LoginPage(),
            )
          ],
          child: MaterialApp(
            routes: const {},
            debugShowCheckedModeBanner: false,
            title: 'Job Search',
            theme: ThemeData(
              textTheme:
                  GoogleFonts.robotoTextTheme(Theme.of(context).textTheme),
              visualDensity: VisualDensity.adaptivePlatformDensity,
              brightness: Brightness.light,
              primaryColor: AppColor.welcomeImageContainer,
              // accentColor: Colors.white.withOpacity(1),
              // accentColorBrightness: Brightness.light),
            ),
            home: FutureBuilder(
                future: checkLoginStatus(),
                builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                  // not LoggedIn
                  if (snapshot.data == true) {
                    return const CheckRole();
                  }
                  return const WelcomeScreen(); // Login UI
                }),
          ),
        );
      },
    );
  }
}
