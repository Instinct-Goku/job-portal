class scheduledInterviewResponse {
  late String job_id;
  late String title;
  late String dateOpen;
  late String userid;
  late String username;

  scheduledInterviewResponse(
      this.job_id, this.title, this.dateOpen, this.userid, this.username);

  scheduledInterviewResponse.fromJson(Map<String, dynamic> json) {
    title = json['job_title'] ?? "";
    job_id = json['job_id'] ?? "";
    dateOpen = json['date_time'] ?? "";
    userid = json['user_id'] ?? "";
    username = json['user_name'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['job_id'] = job_id;
    data['job_title'] = title;
    data['date_time'] = dateOpen;
    data['user_id'] = userid;
    data['user_name'] = username;

    return data;
  }
}
