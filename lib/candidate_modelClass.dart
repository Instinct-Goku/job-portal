class candidate_modelClass {
  late String city;
  late String company;
  late String company_id;
  late String contact_num;
  late String job_id;
  late String jobTitle;
  late String userId;
  late String userName;
  late String high_edu;
  late String skill;
  late String experience;
  late String candidate_status;
  late String email;
  late String resume;
  late String feedback;

  candidate_modelClass(
      this.company_id,
      this.job_id,
      this.company,
      this.city,
      this.jobTitle,
      this.userName,
      this.contact_num,
      this.userId,
      this.high_edu,
      this.skill,
      this.experience,
      this.candidate_status,
      this.email,
      this.resume,
      this.feedback);

  candidate_modelClass.fromJson(Map<String, dynamic> json) {
    city = json['city'] ?? "";
    company = json['company_name'] ?? "";
    userId = json['user_id'] ?? "";
    job_id = json['job_id'] ?? "";
    company_id = json['company_id'] ?? "";
    jobTitle = json['job_title'] ?? "";
    userName = json['user_name'] ?? "";
    contact_num = json['contact_number'].toString() ?? "";
    high_edu = json['education'].toString() ?? "";
    skill = json['skills'].toString() ?? "";
    experience = json['experience'].toString() ?? "";
    candidate_status = json['candidate_status'].toString() ?? "";
    email = json['email'].toString();
    resume = json['resume'].toString() ?? "";
    feedback = json['feedback'].toString() ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['city'] = city;
    data['company'] = company;
    data['user_id'] = userId;
    data['job_id'] = job_id;
    data['company_id'] = company_id;
    data['job_title'] = jobTitle;
    data['user_name'] = userName;
    data['skills'] = skill;
    data['education'] = high_edu;
    data['contact_number'] = contact_num;
    data['email'] = email;
    data['resume'] = resume;
    data['feedback'] = feedback;
    return data;
  }
}
