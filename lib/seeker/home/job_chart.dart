class Job {
  late String industry;
  Job(this.industry);

  Job.fromJSON(Map<String, dynamic> json) {
    industry = json['industry'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['industry'] = industry;

    return data;
  }
}
