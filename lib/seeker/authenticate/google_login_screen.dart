import 'package:flutter/material.dart';
import 'package:job_portal_final/shared/screens.dart'; // Required  Import file
// import 'package:flutter_signin_button/flutter_signin_button.dart'; // Required Packages import
import 'package:sign_button/sign_button.dart';
import 'package:provider/provider.dart';

class GoogleLoginScreen extends StatefulWidget {
  const GoogleLoginScreen({super.key});

  @override
  _GoogleLoginScreenState createState() => _GoogleLoginScreenState();
}

class _GoogleLoginScreenState extends State<GoogleLoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ShaderMask(
          shaderCallback: (rect) => const LinearGradient(
            begin: Alignment.bottomCenter,
            end: Alignment.center,
            colors: [Colors.white, Colors.transparent],
          ).createShader(rect),
          child: Container(
            decoration: const BoxDecoration(color: Colors.white),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.white,
          body: loginUI(),
        )
      ],
    );
  }

  loginUI() {
    // Function that returns UI after checking logged in or not
    return Consumer<GoogleSignInController>(
      builder: (context, model, child) {
        if (model.googleAccount != null) {
          return Center(
            child: loggedInUI(model),
          );
        } else {
          return loginControls(context);
        }
      },
    );
  }

  loggedInUI(GoogleSignInController model) {
    // Logged In UI behaviour
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CircleAvatar(
          backgroundImage:
              Image.network(model.googleAccount!.photoUrl ?? '').image,
          radius: 60.0,
        ),
        Text(model.googleAccount!.displayName ?? ''),
        Text(model.googleAccount!.email),
        ActionChip(
          avatar: const Icon(Icons.logout),
          label: const Text("Logout"),
          onPressed: () {
            Provider.of<GoogleSignInController>(context, listen: false)
                .logout();
          },
        )
      ],
    );
  }

  loginControls(BuildContext context) {
    // Not logged In display signin button
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: const EdgeInsets.all(20),
            alignment: Alignment.center,
            child: const Image(
              image: AssetImage('assets/images/logo.png'),
            ),
          ),
          const Center(
            child: Text(
              'Job Search',
              style: TextStyle(
                  color: Colors.lightBlue,
                  fontSize: 60,
                  fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            child: SignInButton(
              buttonType: ButtonType.google,
              onPressed: () =>
                  Provider.of<GoogleSignInController>(context, listen: false)
                      .login(),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            child: SignInButton(
              buttonType: ButtonType.mail,
              btnText: 'Sign Up with Email',
              onPressed: () => Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (context) => const SignInScreen(),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
