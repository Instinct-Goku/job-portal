class myJobsResponse {
  late String city;
  late String company;
  late String company_id;
  late String job_id;
  late String jobTitle;
  late String candidate_status;
  late String feedback;

  myJobsResponse(this.company_id, this.job_id, this.company, this.city,
      this.jobTitle, this.candidate_status, this.feedback);

  myJobsResponse.fromJson(Map<String, dynamic> json) {
    city = json['city'] ?? "";
    company = json['company_name'] ?? "";
    job_id = json['job_id'] ?? "";
    company_id = json['company_id'] ?? "";
    jobTitle = json['job_title'] ?? "";
    candidate_status = json['candidate_status'].toString() ?? "";
    feedback = json['feedback'].toString() ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['city'] = city;
    data['company'] = company;
    data['job_id'] = job_id;
    data['company_id'] = company_id;
    data['job_title'] = jobTitle;
    data['candidate_status'] = candidate_status;
    data['feedback'] = feedback;

    return data;
  }
}
